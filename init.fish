starship init fish | source
neofetch
#source $HOME/.cargo/env
set -g man_blink -o blue
set -g man_bold -o red
set -g man_standout -b black 93a1a1
set -g man_underline -u green
