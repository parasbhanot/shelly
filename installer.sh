#!/usr/bin/bash

mkdir -p ~/.config/omf

curl -L https://get.oh-my.fish | fish

sleep 60s

curl -fsSL https://starship.rs/install.sh | bash
